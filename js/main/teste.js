{
  const quadrado = {
    lados: 4,
    area(lado) {
      return lado * lado;
    },
    perimetro(lado) {
      return this.lados * lado;
    },

    ladoAreaPerimetro(lado) {
      return `${`${this.lados}` + ' '}${this.area(lado)} ${this.perimetro(lado)}`;
    },
  };

  const cachorro = {
    raca: 'Labrador',
    cor: 'Preto',
    idade: 10,
    latir(pessoa) {
      console.log(pessoa === 'homem' ? 'Latiu' : 'Nada');
    },
  };
  console.log(cachorro);
  console.log(quadrado.area(5));
  console.log(quadrado.ladoAreaPerimetro(5));
}
