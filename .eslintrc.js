module.exports = {
  env: {
    browser: true,
    es6: true,
    node: false,
  },
  extends: [
    'airbnb-base',
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  rules: {
    "no-console": 0,
    "no-useless-concat": 0,
    "no-undef": 0
  },
};
