// Adiciona os modulos instalados
const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const browsersync = require('browser-sync').create();
const concat = require('gulp-concat');
const babel = require('gulp-babel');
const uglify = require('gulp-uglify');

// Funcao para compilar o SASS e adicionar os prefixos
function compilasass() {
  return gulp
    .src('css/scss/*.scss')
    .pipe(sass({
      outputStyle: 'compressed',
    }))
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false,
    }))
    .pipe(gulp.dest('css/'))
    .pipe(browsersync.stream());
}

// Tarefa do Gulp para a funcao de SASS
gulp.task('sass', compilasass);

// Funcao para juntar o js
function gulpJs() {
  return gulp
    .src('js/main/*.js')
    .pipe(concat('main.js'))
    .pipe(babel({
      presets: ['@babel/env'],
    }))
    .pipe(uglify())
    .pipe(gulp.dest('js/'))
    .pipe(browsersync.stream());
}

// Tarefa do Gulp para juntar todos os js, comprimir e etc
gulp.task('mainjs', gulpJs);

// JS Plugins
function pluginJS() {
  return gulp.src([
    'node_modules/jquery/dist/jquery.min.js',
    'node_modules/moment/min/moment.min.js',
    'js/plugins/*.js',
  ])
    .pipe(concat('plugins.js'))
    .pipe(uglify())
    .pipe(gulp.dest('js/'))
    .pipe(browsersync.stream());
}

gulp.task('pluginjs', pluginJS);


// Funcao para iniciar o browser
function browser() {
  browsersync.init({
    server: {
      baseDir: './',
    },
  });
}

// Tarefa para iniciar o browsersync
gulp.task('browser-sync', browser);

// Funcao de Watch do gulp
function watch() {
  gulp.watch('css/scss/*.scss', compilasass);
  gulp.watch('js/main/*.js', gulpJs);
  gulp.watch('js/plugins/*.js', pluginJS);
  gulp.watch(['*.html', '*.php', '*.cshtml']).on('change', browsersync.reload);
}

// Inicia a tarefa de Watch
gulp.task('watch', watch);

// Inicia as tarefas padrao do Gulp
gulp.task('default', gulp.parallel('watch', 'browser-sync', 'sass', 'mainjs'));
